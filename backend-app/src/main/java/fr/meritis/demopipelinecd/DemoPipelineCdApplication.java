package fr.meritis.demopipelinecd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoPipelineCdApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoPipelineCdApplication.class, args);
	}

}
